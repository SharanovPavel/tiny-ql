﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Newtonsoft.Json.Linq;
using TinyQL.Tests.Fixtures;
using Xunit;

namespace TinyQL.Tests
{
    public class QueryTests
    {
        [Fact]
        public void String_equal_query()
        {
            // Arrange
            var json = @"{""Name"": {""$eq"": ""Pavel""}}";
            var source = new List<Person>
            {
                new Person {Name = "Pavel"},
                new Person {Name = "SANYOK"}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }

        [Fact]
        public void String_not_equal_query()
        {
            // Arrange
            var json = @"{""Name"": {""$neq"": ""SANYOK""}}";
            var source = new List<Person>
            {
                new Person {Name = "Pavel"},
                new Person {Name = "SANYOK"}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }

        [Fact]
        public void Number_equal_query()
        {
            // Arrange
            var json = @"{""Age"": {""$eq"": 25}}";
            var source = new List<Person>
            {
                new Person {Name = "Pavel", Age = 25},
                new Person {Name = "SANYOK", Age = 26}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }

        [Fact]
        public void String_contains_query()
        {
            // Arrange
            var json = @"{""Name"": {$in: [""Pavel"", ""Nekit""]}}";
            var source = new List<Person>
            {
                new Person {Name = "Pavel", Age = 25},
                new Person {Name = "SANYOK", Age = 26}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }

        [Fact]
        public void Number_contains_query()
        {
            // Arrange
            var json = @"{""Age"": {$in: [25, 27]}}";
            var source = new List<Person>
            {
                new Person {Name = "Pavel", Age = 25},
                new Person {Name = "SANYOK", Age = 26}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }

        [Fact]
        public void DateTime_contains_query()
        {
            // Arrange
            var json = @"{""dateOfBirth"": {$in: [""1995-12-10"", ""1997-07-23""]}}";
            var source = new List<Person>
            {
                new() {Name = "Pavel", Age = 25, DateOfBirth = new DateTime(1995, 12, 10)},
                new() {Name = "SANYOK", Age = 26, DateOfBirth = new DateTime(1995, 11, 2)}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }

        [Fact]
        public void Query_several_fields()
        {
            // Arrange
            var json =
                @"{""dateOfBirth"": {$in: [""1995-12-10"", ""1997-07-23""]}, ""name"": ""Pavel"", ""age"": {""$lte"": 25}}";
            var source = new List<Person>
            {
                new() {Name = "Pavel", Age = 25, DateOfBirth = new DateTime(1995, 12, 10)},
                new() {Name = "SANYOK", Age = 26, DateOfBirth = new DateTime(1995, 11, 2)}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }

        [Fact]
        public void Or_query()
        {
            // Arrange
            var json = @"{""$or"": [{""age"": 30}, {""name"": ""Pavel""}]}";
            var source = new List<Person>
            {
                new() {Name = "Pavel", Age = 25, DateOfBirth = new DateTime(1995, 12, 10)},
                new() {Name = "SANYOK", Age = 26, DateOfBirth = new DateTime(1995, 11, 2)}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }

        [Fact]
        public void And_query()
        {
            // Arrange
            var json = @"{""$or"": [{""age"": 30}, {""name"": ""Pavel""}]}";
            var source = new List<Person>
            {
                new() {Name = "Pavel", Age = 25, DateOfBirth = new DateTime(1995, 12, 10)},
                new() {Name = "SANYOK", Age = 26, DateOfBirth = new DateTime(1995, 11, 2)}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }

        [Fact]
        public void Or_field_query()
        {
            // Arrange
            var json = @"{""name"": {""$or"": [{""$eq"": ""Pavel""}, {""$eq"": ""Nekit""}]}}";
            var source = new List<Person>
            {
                new() {Name = "Pavel", Age = 25, DateOfBirth = new DateTime(1995, 12, 10)},
                new() {Name = "SANYOK", Age = 26, DateOfBirth = new DateTime(1995, 11, 2)}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }

        [Fact]
        public void Nested_property_query()
        {
            // Arrange
            var json = @"{""boss"": {""name"": ""Marin""}}";
            var source = new List<Person>
            {
                new()
                {
                    Name = "Pavel", Age = 25, DateOfBirth = new DateTime(1995, 12, 10),
                    Boss = new Person {Name = "Marin"}, Subordinates = new List<Person> {new() {Name = "Nikita"}}
                },
                new() {Name = "SANYOK", Age = 26, DateOfBirth = new DateTime(1995, 11, 2), Boss = new Person()}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }
        
        [Fact]
        public void Any_operator_query()
        {
            // Arrange
            var json = @"{""subordinates"": {""$any"": {""name"": ""Nikita""}}}";
            var source = new List<Person>
            {
                new() {Name = "Pavel", Subordinates = new List<Person> {new() {Name = "Nikita"}}},
                new() {Name = "SANYOK", Subordinates = new List<Person> {new () {Name = "Evgeniy"}}}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }

        [Fact]
        public void Count_operator_query()
        {
            // Arrange
            var json = @"{""subordinates"": {""$count"": {""$eq"": 2, ""$gte"": 2}}}";
            var source = new List<Person>
            {
                new() {Name = "Pavel", Subordinates = new List<Person> {new() {Name = "Nikita"}, new () {Name = "Name"}}},
                new() {Name = "SANYOK", Subordinates = new List<Person> {new () {Name = "Evgeniy"}}}
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(1);
            result.Should().ContainSingle(e => e.Name == "Pavel");
        }

        [Fact]
        public void Complex_count_operator_query()
        {
            // Arrange
            var json = @"{""subordinates"": {""$count"": {""$or"": [{""$eq"": 1}, {""$eq"": 2}]}}}";
            var source = new List<Person>
            {
                new() { Name = "Pavel", Subordinates = new List<Person> {new() {Name = "Nikita"}, new () {Name = "Name"}} },
                new() { Name = "SANYOK", Subordinates = new List<Person> {new () {Name = "Evgeniy"}}},
                new () { Subordinates = new List<Person>() }
            }.AsQueryable();

            // Act
            var expression = Query.ParseExpression<Person>(json);
            var result = source.Where(expression).ToList();

            // Assert
            result.Should().HaveCount(2);
        }
    }
}