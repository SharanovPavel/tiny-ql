﻿using System;
using System.Collections.Generic;

namespace TinyQL.Tests.Fixtures
{
    public class Person
    {
        public string Name { get; set; }
        
        public int Age { get; set; }
         
        public DateTime DateOfBirth { get; set; }
        
        public Person Boss { get; set; }
        
        public ICollection<Person> Subordinates { get; set; } 
    }
}