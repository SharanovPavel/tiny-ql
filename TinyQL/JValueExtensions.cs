﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using Newtonsoft.Json.Linq;
using TinyQL.Reflection;

namespace TinyQL
{
    public static class JValueExtensions
    {
        public static object Value(this JValue value, Type type)
        {
            return ValueMethodInfoCache.GetMethod(type).Invoke(null, new object[] {value});
        }
    }
}