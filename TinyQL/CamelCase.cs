﻿using System;

namespace TinyQL
{
    public static class CamelCase
    {
        public static string ToPascalCase(string camelCaseString)
        {
            if (camelCaseString == null)
                throw new ArgumentNullException(nameof(camelCaseString));

            return camelCaseString.Length switch
            {
                0 => camelCaseString,
                1 => camelCaseString.ToUpperInvariant(),
                _ => char.ToUpperInvariant(camelCaseString[0]) + camelCaseString.Substring(1)
            };
        }
    }
}