﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;

namespace TinyQL.Reflection
{
    public static class AnyMethodInfoCache
    {
        private static readonly MethodInfo AnyMethod = typeof(Enumerable).GetMethods()
            .First(method => method.Name == "Any" && method.GetParameters().Length == 2);

        private static readonly ConcurrentDictionary<Type, MethodInfo> Cache = new();
        
        public static MethodInfo GetAnyMethod(Type elementType)
        {
            return Cache.GetOrAdd(elementType, MakeGenericAny);
        }

        private static MethodInfo MakeGenericAny(Type elementType) => AnyMethod.MakeGenericMethod(elementType);
    }
}