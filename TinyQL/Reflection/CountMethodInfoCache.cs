﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TinyQL.Reflection
{
    public static class CountMethodInfoCache
    {
        private static readonly MethodInfo CountMethod = typeof(Enumerable).GetMethods()
            .First(method => method.Name == "Count" && method.GetParameters().Length == 1);

        private static ConcurrentDictionary<Type, MethodInfo> Cache = new();
        
        public static MethodInfo GetCountMethod(Type elementType)
        {
            return Cache.GetOrAdd(elementType, CreateCountMethod);
        }

        private static MethodInfo CreateCountMethod(Type elementType) => CountMethod.MakeGenericMethod(elementType);
    }
}