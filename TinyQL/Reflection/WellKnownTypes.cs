﻿using System;

namespace TinyQL.Reflection
{
    public static class WellKnownTypes
    {
        public static readonly Type Int32 = typeof(int);
    }
}