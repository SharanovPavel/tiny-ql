﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace TinyQL.Reflection
{
    internal static class ValueMethodInfoCache
    {
        private static readonly MethodInfo Method =
            typeof(Extensions).GetMethod(nameof(Extensions.Value), 1, new [] {typeof(IEnumerable<JToken>)});
        
        private static readonly ConcurrentDictionary<Type, MethodInfo> Cache = new();

        public static MethodInfo GetMethod(Type type)
        {
            return Cache.GetOrAdd(type, CreateMethodInfo);
        }

        private static MethodInfo CreateMethodInfo(Type type)
        {
            return Method.MakeGenericMethod(type);
        }
    }
}