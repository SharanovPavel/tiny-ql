﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace TinyQL
{
    public static class Field
    {
        private static readonly Regex FieldRegex = new Regex("^[a-zA-Z]+$");
        public static bool IsField(string name) => FieldRegex.IsMatch(name);

        public static Expression ParsePropertyExpression(Expression expression, JProperty jProperty)
        {
            var propertyName = CamelCase.ToPascalCase(jProperty.Name);
            var property = Expression.Property(expression, propertyName);

            // todo unsafe??
            var type = (property.Member.MemberType & MemberTypes.Property) != 0
                ? ((PropertyInfo) property.Member).PropertyType
                : throw new Exception("BOOM!");
            
            return jProperty.Value switch
            {
                JObject obj => Query.ParseQueryObject(property, type, obj),
                JValue value => Operator.Equal(property, type, value),
                _ => throw new Exception("Unexpected token.")
            };
        }
    }
}