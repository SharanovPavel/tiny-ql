﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Newtonsoft.Json.Linq;
using TinyQL.Reflection;

namespace TinyQL
{
    public static class Operator
    {
        public static bool IsOperator(string name) => name.StartsWith("$");

        public static Expression ParsePropertyExpression(Expression expression, Type type, JProperty jProperty)
        {
            return jProperty.Name switch
            {
                "$eq" => Equal(expression, type, (JValue) jProperty.Value),
                "$neq" => NotEqual(expression, type, (JValue) jProperty.Value),
                "$in" => In(expression, type, (JArray) jProperty.Value),
                "$lt" => LessThan(expression, type, (JValue) jProperty.Value),
                "$lte" => LessThanOrEqual(expression, type, (JValue) jProperty.Value),
                "$gt" => GreaterThan(expression, type, (JValue) jProperty.Value),
                "$gte" => GreaterThanOrEqual(expression, type, (JValue) jProperty.Value),
                "$and" => And(expression, type, (JArray)jProperty.Value),
                "$or" => Or(expression, type, (JArray)jProperty.Value),
                "$any" => Any(expression, type, (JObject)jProperty.Value),
                "$count" => Count(expression, type, (JObject)jProperty.Value),
                _ => throw new Exception("Unknown operator.")
            };
        }

        private static Expression LogicalOperator(Expression expression, Type type, JArray array,
            Func<Expression, Expression, Expression> operatorExpression)
        {
            return array.Children<JObject>().Select(x => Query.ParseQueryObject(expression, type, x))
                .Aggregate(operatorExpression);
        }

        private static Expression ComparisionOperator(Expression expression, Type type, JValue value,
            Func<Expression, Expression, Expression> operatorExpression)
        {
            return operatorExpression(expression, Value.Parse(value, type));
        }
        
        public static Expression Count(Expression expression, Type type, JObject jObject)
        {
            var elementType = type.GetGenericArguments()[0];
            var countMethod = CountMethodInfoCache.GetCountMethod(elementType);
            var countCall = Expression.Call(null, countMethod, expression);
            return Query.ParseQueryObject(countCall, WellKnownTypes.Int32, jObject);
        }
        
        public static Expression Any(Expression expression, Type type, JObject jObject)
        {
            var elementType = type.GetGenericArguments()[0];
            var anyMethod = AnyMethodInfoCache.GetAnyMethod(elementType);
            return Expression.Call(null, anyMethod, expression, Query.ParseLambda(elementType, jObject));
        }
        
        public static Expression Or(Expression expression, Type type, JArray jArray)
        {
            return LogicalOperator(expression, type, jArray, Expression.OrElse);
        }

        public static Expression And(Expression expression, Type type, JArray array)
        {
            return LogicalOperator(expression, type, array, Expression.AndAlso);
        }

        public static Expression GreaterThanOrEqual(Expression expression, Type type, JValue value)
            => ComparisionOperator(expression, type, value, Expression.GreaterThanOrEqual);

        public static Expression GreaterThan(Expression expression, Type type, JValue value)
            => ComparisionOperator(expression, type, value, Expression.GreaterThan);

        public static Expression Equal(Expression expression, Type type, JValue value)
            => ComparisionOperator(expression, type, value, Expression.Equal);

        public static Expression NotEqual(Expression expression, Type type, JValue value)
            => ComparisionOperator(expression, type, value, Expression.NotEqual);

        public static Expression In(Expression expression, Type type, JArray value)
        {
            var method = typeof(List<>).MakeGenericType(type).GetMethod("Contains");
            return Expression.Call(ArrayValue.ParseValues(value, type), method!, expression);
        }

        public static Expression LessThanOrEqual(Expression expression, Type type, JValue value)
            => ComparisionOperator(expression, type, value, Expression.LessThanOrEqual);

        public static Expression LessThan(Expression expression, Type type, JValue value)
            => ComparisionOperator(expression, type, value, Expression.LessThan);
    }
}