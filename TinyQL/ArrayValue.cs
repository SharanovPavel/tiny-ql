﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Newtonsoft.Json.Linq;

namespace TinyQL
{
    public static class ArrayValue
    {
        public static List<T> MapEnumerable<T>(JEnumerable<JValue> values)
        {
            return values.Select(x => x.Value<T>()).ToList();
        }
        
        public static Expression ParseValues(JArray array, Type elementType)
        {
            // todo make method MapEnumerable private
            var method = typeof(ArrayValue).GetMethod(nameof(MapEnumerable))!.MakeGenericMethod(elementType);
            var values = array.Children<JValue>();
            return Expression.Constant(method.Invoke(null, new object[]{values}));
        }
            
    }
}