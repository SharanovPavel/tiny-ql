﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Newtonsoft.Json.Linq;

namespace TinyQL
{
    public static class Query
    {
        public static Expression<Func<T, bool>> ParseExpression<T>(string json)
        {
            var jObject = JObject.Parse(json);
            return ParseExpression<T>(jObject);
        }

        public static Expression<Func<T, bool>> ParseExpression<T>(JObject jObject)
        {
            var type = typeof(T);
            return (Expression<Func<T, bool>>)ParseLambda(type, jObject);
        }

        internal static Expression ParseLambda(Type type, JObject jObject)
        {
            var parameter = Expression.Parameter(type, "arg");
            return ParseLambda(parameter, type, jObject);
        }

        private static Expression ParseLambda(ParameterExpression parameter, Type type, JObject jObject)
        {
            var lambdaBody = ParseQueryObject(parameter, type, jObject);
            return Expression.Lambda(lambdaBody, parameter);
        }
        
        internal static Expression ParseQueryObject(Expression expression, Type type, JObject jobj)
        {
            return jobj.Properties()
                .Select(property => ParsePropertyExpression(expression, type, property))
                .Aggregate(Expression.And);
        }

        private static Expression ParsePropertyExpression(Expression expression, Type type, JProperty property)
        {
            if (Operator.IsOperator(property.Name))
                return Operator.ParsePropertyExpression(expression, type, property);

            if (Field.IsField(property.Name))
                return Field.ParsePropertyExpression(expression, property);

            throw new Exception();
        }
    }
}