﻿using System;
using System.Linq.Expressions;
using Newtonsoft.Json.Linq;

namespace TinyQL
{
    public static class Value
    {
        public static Expression Parse(JValue value, Type type)
        {
            return Expression.Constant(value.Value(type));
        }
    }
}